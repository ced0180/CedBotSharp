﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Discord.WebSocket;
using DotNetEnv;
using Microsoft.Win32;

namespace CedBotSharp
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        private Extensions.MutexHandler mh;
        private Extensions.DiscordRPC rpc;
        private Extensions.NotifyIconWrapper niw;
        private Bot.CedBot bot;
        private DrawingAttributes inkDA = new DrawingAttributes();
        private OtherWindows.AboutBox1 about = new OtherWindows.AboutBox1();
        private OtherWindows.SettingWindow setting = new OtherWindows.SettingWindow();
        private OtherWindows.SendToolWindow sendTool;

        public MainWindow()
        {
            Env.Load(Properties.Settings.Default.envFile);
            mh = new Extensions.MutexHandler();

            InitializeComponent();

            Loaded += (s, e) =>
            {
                niw = new Extensions.NotifyIconWrapper(this);
                bot = new Bot.CedBot(this, Env.GetString("token"));
                rpc = new Extensions.DiscordRPC(this, Properties.Settings.Default.drpcAppID);

                sendTool = new OtherWindows.SendToolWindow(bot);

                PaintColor.SelectedColor = PaintCanvas.DefaultDrawingAttributes.Color;
            };

            Closing += (s, e)=>
            {
                e.Cancel = true;
                Hide();
            };
        }

        public void WriteLog(string title, string text)
        {
            LogBox.AppendText("[" + title + " - " + System.DateTime.Now.ToString() + "]" + text + "\r\n");
            LogBox.ScrollToEnd();
        }

        public void ExitEventHandler(object s)
        {
            if (!Properties.Settings.Default.isClosing) return;
            MessageBoxResult result = MessageBox.Show("When this window is closed, the running bot will be stopped.",
                "CedBot# - Exit?",
                MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                mh.mutex.ReleaseMutex();
                mh.mutex.Close();
                niw.Dispose();
                Application.Current.Shutdown();
            };
        }

        private void ExitItem_Click(object sender, RoutedEventArgs e)
        {
            ExitEventHandler(sender);
        }

        private void SettingItem_Click(object sender, RoutedEventArgs e)
        {
            setting.ShowDialog();
        }

        private void AboutItem_Click(object sender, RoutedEventArgs e)
        {
            about.ShowDialog();
        }

        private void CopyBotLinkItem_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText($"https://discord.com/oauth2/authorize?client_id={bot.client.CurrentUser.Id}&permissions=2218126400&scope=bot");
            MessageBoxResult res = MessageBox.Show("I copied the invite link.", "CedBot# - Clipboard");
        }

        private void LogReset_Click(object sender, RoutedEventArgs e)
        {
            LogBox.Text = "";
        }

        private void GuildListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (GuildListView.SelectedItem == null) return;
            SocketGuild item = (SocketGuild)GuildListView.SelectedItem;

            GuildIconImage.Source = string.IsNullOrWhiteSpace(item.IconUrl) ?
                new BitmapImage(new Uri("pack://application:,,,/GuildIconNotFound.png")) :
                new BitmapImage(new Uri(item.IconUrl));
            GuildNameLabel.Content = "Name: " + item.Name;
            GuildIDLabel.Content = "ID: " + item.Id;

            
        }

        private void SendToolButton_Click(object sender, RoutedEventArgs e)
        {
            sendTool.ShowDialog();
        }

        private void PaintColor_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            Color penColor = new Color();
            penColor = e.NewValue.Value;
            inkDA.Color = penColor;
            PaintCanvas.DefaultDrawingAttributes = inkDA;
        }

        private void PaintSize_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            inkDA.Width = e.NewValue;
            inkDA.Height = e.NewValue;

            PaintCanvas.DefaultDrawingAttributes = inkDA;
        }

        private void PaintCanvasReset_Click(object sender, RoutedEventArgs e)
        {
            PaintCanvas.Strokes.Clear();
        }

        private void PaintCanvasSave_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            DateTime date = DateTime.Now;
            string dateFormat = $"{date.Year}-{date.Month}-{date.Day} {date.Hour}-{date.Minute}-{date.Second}";
            dlg.FileName = "CedBotSharp" + dateFormat;
            dlg.DefaultExt = ".png";
            dlg.Filter = "Image (.png)|*.png";

            bool? result = dlg.ShowDialog();

            if (result == true)
            {
                string filename = dlg.FileName;

                PaintCanvas.toImage(filename);
            }
        }
    }
}

public static class InkCanvasExtensions
{
    public static void toImage(this InkCanvas canvas, string path)
    {
        var size = new Size(canvas.ActualWidth, canvas.ActualHeight + canvas.Margin.Bottom);
        canvas.Measure(size);
        canvas.Arrange(new Rect(size));

        var renderBitmap = new RenderTargetBitmap((int)size.Width,
            (int)size.Height,
            96.0d,
            96.0d,
            PixelFormats.Pbgra32);
        renderBitmap.Render(canvas);

        using (var os = new FileStream(path, FileMode.Create))
        {
            BitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(renderBitmap));
            encoder.Save(os);
        }
    }
}