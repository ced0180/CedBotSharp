﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using Discord.Commands;
using Microsoft.Extensions.DependencyInjection;
using CedBotSharp.Bot.Services;
using System.Net.Http;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Reflection;

namespace CedBotSharp.Bot
{
    public class CedBot
    {
        private MainWindow window;
        public DiscordSocketClient client;

        public CedBot(MainWindow window, string token)
        {
            this.window = window;
            _ = MainAsync(token);
        }

        public async Task MainAsync(string token)
        {
            using (var services = ConfigureServices())
            {
                client = services.GetRequiredService<DiscordSocketClient>();
                var _client = services.GetRequiredService<DiscordSocketClient>();

                _client.Log += LogAsync;
                _client.Ready += ReadyAsync;
                _client.JoinedGuild += JoinedGuildAsync;
                _client.LeftGuild += LeftGuildAsync;
                services.GetRequiredService<CommandService>().Log += LogAsync;

                await _client.LoginAsync(TokenType.Bot, token);
                await _client.StartAsync();

                await services.GetRequiredService<CommandHandlingService>().InitializeAsync();

                await Task.Delay(Timeout.Infinite);
            }
        }

        private Task ReadyAsync()
        {
            Console.WriteLine($"{client.CurrentUser} is connected!");
            client.SetStatusAsync(UserStatus.Online);
            client.SetGameAsync($"Running on .Net {Environment.Version}");

            window.Dispatcher.Invoke(new Action(() => {
                window.WriteLog("BOT", $"{client.CurrentUser} is connected!");
                window.Title = $"CedBot# GUI - {client.CurrentUser}";

                window.GuildListView.ItemsSource = client.Guilds;
            }));

            return Task.CompletedTask;
        }

        private Task LogAsync(LogMessage log)
        {
            Console.WriteLine(log.ToString());

            window.Dispatcher.Invoke(new Action(() => {
                window.WriteLog("Discord.Net", log.ToString());
            }));

            return Task.CompletedTask;
        }

        private Task LeftGuildAsync(SocketGuild arg)
        {
            window.Dispatcher.Invoke(new Action(() => {
                SocketGuild item = (SocketGuild)window.GuildListView.SelectedItem;
                if (arg.Id == item.Id)
                {
                    window.GuildIconImage.Source = null;
                    window.GuildNameLabel.Content = "Name: ";
                    window.GuildIDLabel.Content = "ID: ";
                }

                window.GuildListView.ItemsSource = client.Guilds;
            }));

            return Task.CompletedTask;
        }

        private Task JoinedGuildAsync(SocketGuild arg)
        {
            window.Dispatcher.Invoke(new Action(() => {
                window.GuildListView.ItemsSource = client.Guilds;
            }));

            return Task.CompletedTask;
        }

        private ServiceProvider ConfigureServices()
        {
            return new ServiceCollection()
                .AddSingleton<DiscordSocketClient>()
                .AddSingleton<CommandService>()
                .AddSingleton<CommandHandlingService>()
                .AddSingleton<HttpClient>()
                .AddSingleton<PictureService>()
                .BuildServiceProvider();
        }
    }
}
