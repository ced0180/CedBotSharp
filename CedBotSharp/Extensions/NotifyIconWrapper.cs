﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace CedBotSharp.Extensions
{
    public partial class NotifyIconWrapper : Component
    {
        public NotifyIconWrapper(MainWindow win)
        {
            InitializeComponent();

            NotifyIcon.MouseClick += (s, e) =>
            {
                if (e.Button == MouseButtons.Left)
                {
                    win.Show();
                }
            };

            NotifyMenuItem_Show.Click += (s, e) => win.Show();
            NotifyMenuItem_Exit.Click += (s, e) => win.ExitEventHandler(s);
        }

        public NotifyIconWrapper(IContainer container)
        {
            container.Add(this);
            InitializeComponent();
        }
    }
}
