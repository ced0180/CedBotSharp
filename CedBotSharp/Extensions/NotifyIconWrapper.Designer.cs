﻿
namespace CedBotSharp.Extensions
{
    partial class NotifyIconWrapper
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NotifyIconWrapper));
            this.NotifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.TrayMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.NotifyMenuItem_Show = new System.Windows.Forms.ToolStripMenuItem();
            this.NotifyMenuItem_Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.TrayMenu.SuspendLayout();
            // 
            // NotifyIcon
            // 
            this.NotifyIcon.ContextMenuStrip = this.TrayMenu;
            this.NotifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("NotifyIcon.Icon")));
            this.NotifyIcon.Text = "CedBot#";
            this.NotifyIcon.Visible = true;
            // 
            // TrayMenu
            // 
            this.TrayMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NotifyMenuItem_Show,
            this.NotifyMenuItem_Exit});
            this.TrayMenu.Name = "TrayMenu";
            this.TrayMenu.Size = new System.Drawing.Size(104, 48);
            // 
            // NotifyMenuItem_Show
            // 
            this.NotifyMenuItem_Show.Name = "NotifyMenuItem_Show";
            this.NotifyMenuItem_Show.Size = new System.Drawing.Size(103, 22);
            this.NotifyMenuItem_Show.Text = "Show";
            // 
            // NotifyMenuItem_Exit
            // 
            this.NotifyMenuItem_Exit.Name = "NotifyMenuItem_Exit";
            this.NotifyMenuItem_Exit.Size = new System.Drawing.Size(103, 22);
            this.NotifyMenuItem_Exit.Text = "Exit";
            this.TrayMenu.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NotifyIcon NotifyIcon;
        private System.Windows.Forms.ContextMenuStrip TrayMenu;
        private System.Windows.Forms.ToolStripMenuItem NotifyMenuItem_Show;
        private System.Windows.Forms.ToolStripMenuItem NotifyMenuItem_Exit;
    }
}
