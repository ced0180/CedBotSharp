﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiscordRPC;

namespace CedBotSharp.Extensions
{
    class DiscordRPC
    {
        public DiscordRPC(MainWindow window, string AppID)
        {
            DiscordRpcClient drpc = new DiscordRpcClient(AppID);
            RichPresence rp = new RichPresence();
            Assets assets = new Assets();

            drpc.OnReady += (s, e) =>
            {
                window.Dispatcher.Invoke(new Action(() => {
                    window.WriteLog("DiscordRPC", $"{drpc.CurrentUser} is connected!");
                }));
            };

            assets.LargeImageKey = "originalicon";
            assets.LargeImageText = "CedBot# By. ced0180 and DetdaNet";
            assets.SmallImageKey = "ced";
            assets.SmallImageText = "what?";

            rp.WithDetails($"RPC on CedBot#(.Net {Environment.Version})");
            rp.WithState("C# is hard but fun~!");
            rp.WithAssets(assets);
            drpc.SetPresence(rp);
            drpc.Initialize();
        }
    }
}
