﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace CedBotSharp.Extensions
{
    class MutexHandler
    {
        public Mutex mutex;

        public MutexHandler()
        {
            var asmb = Assembly.GetExecutingAssembly().GetName();
            mutex = new Mutex(false, $"DN-CBS - {asmb.Name}");

            if (!mutex.WaitOne(0, false))
            {
                MessageBox.Show("This application cannot be launched multiple times.",
                    "CedBot#",
                    MessageBoxButton.OK, MessageBoxImage.Warning);
                mutex.Close();
                mutex = null;
                Application.Current.Shutdown();
            }
        }
    }
}
